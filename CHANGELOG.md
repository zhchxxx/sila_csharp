# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and starting with version number 1.0.0 this project will adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The version numbers in this changelog refer to all SiLA2 assemblies and their nuget packages (guaranteed to match).


## Current Nuget Version is 7.4.5

## Version 7.4.5
- BREAKING CHANGE: Interface change of SiLA2.AspNetCore.KestrelExtensions.SetupKestrel due to deprecation of Nuget Package Microsoft.AspNetCore.Server.Kestrel.Core which was moved into namespace Microsoft.AspNetCore.Hosting. Method SiLA2.AspNetCore.KestrelExtensions.SetupKestrel was replaced by SiLA2.AspNetCore.KestrelExtensions.GetKestrelConfigData.
- Storing AnIML Series in NoSQL Database as BSON (Binary JSON) and converting it back to Xsd validated AnIML
- ICertificateContext: Setting variable path of certificate containing folder
- TemperatureController Example: RazorPages were extracted from SiLA2.Temperature.Server.App into the dedicated web project SiLA2.Temperature.Server.App.Webfrontend

## Version 7.4.5
- CertificateContext is registered in IoC Container with ICertificateContext

## Version 7.4.2
- All 191 Integration Tests triggered by Python Client from Repo https://gitlab.com/SiLA2/sila_interoperability pass
- SiLA2.AspNetCore
    - Server Bootstrapping: Set up IP from ServerConfig 
	- Implementation of Error Handling in AuthenticationService & AuthorizationService + Integration Tests

## Version 7.3.2
- SiLA2
	- Extended ErrorHandling.RaiseSiLAError with Metadata
- SiLA2.Utils
	- Password Hashing Algorithm updated (Breaking change: renamed PasswordHash to PasswordHashService)
	

## version 7.3.1
- Implemented Authentication & Authorization + basic Example
- Published new gRPC-Service-Stubs for missing SiLA2.Core-Feartures
    - AuthorizationConfigurationService
	- AuthorizationProviderService
	- SimulationController
	- CancelController
	- PauseController
	- ParameterConstraintsProvider 

## version 7.2.1
- Removed GrpcChannel from ServerData.cs
- Removed GrpcServerChannelProvider.cs

## version 7.1.3
- Changed Signature of SiLA2.ServiceFinder.GetConnectionsContinuously

## version 7.1.2
- Removed unnecessary Dependency from ServerDataProvider & SiLA2.Client

## version 7.1.1
- AspNetCore Dependency was removed from SiLA2.Core into new Module SiLA2.AspNet.Core
- Implementiation of ConnectionConfigurationService
- Preparation of ServerInitiated Connection Communication by CloudEndPoint 

## version 7.0.0
- Updated entire Solution to .NET 7

## Version 6.4.1
- BREAKING CHANGE: IBinaryDataRepository was split into IBinaryUploadRepository and IBinaryDownloadRepository
- Changes in ObservableCommandManager/ObservableCommandWrapper e.g. IntermediateResponses implementation 

## Version 6.3.9
- BREAKING CHANGE: Changed Namespace of SiLA2.Server.Database in SiLA2.Database.SQL because the module could be used on client side as well
- Added Module SiLA2.Database.NoSQL as Document Database Addin
	- generated AnIML-Code is easier to be managed there compared to a relational database
	- open issue: conversion from BSON/JSON back to AnIML-XML
- Repository for AnIML.Core- und AnIML.Technique-Data 

## SiLA2.Core 6.3.6 & SiLA2.Feature Assemblies
- Core-Features LockController-v2_0.sila.xml and ErrorRecoveryService-v1_0.sila.xml are not active by default
	- they have to be explicitly activated in the SiLA2.Server.App or in its SiLA2.Feature-Assembly 
	- if LockController-Feature is not found it would be read from file in LockControllerService-Constructor
- Web-Application SiLA2.Server.Manager
	- Monitor active SiLA2-Servers found by mDNS-Service
	- Show Features of found Servers
	- In progress -> creating dynamic SiLA2-Clients to control SiLA2-Servers


## SiLA2.Core 6.3.5
- LockControllerService: IsLocked Property is now observable based on LockController-v2_0.sila.xml
- CI-Pipline runs from now Job 'integration-test-job-python' on SiLA2.IntegrationTests.ServerApp with Python Client 
- Namespace changes: 
  - SiLA2.Utils.KestrelExtensions -> SiLA2.Server.KestrelExtensions
  - SiLA2.Utils.AnyType -> SiLA2.Domain.AnyType

## SiLA2.Frontend.Razor 6.3.1
- Page to manage Server Certificates 

## SiLA2.Utils 6.3.1
- CertificateContext, CertificateProvider, CertificateRepository

## SiLA2 & SiLA2.Utils 6.2.1
- Creating, loading and saving Server Certificate & Key for TLS at runtime 

## SiLA2.Server.Database [6.0.2] -> [6.0.3]
- BREAKING CHANGE:  Changed DataType of generic BaseEntity Key from INT to GUID

## SiLA2.Server [6.0.6] -> SiLA2.Core [6.0.7]
- BREAKING CHANGE:  Changed Nuget-Package-Name of SiLA2.csproj from 'SiLA2.Server' to 'SiLA2.Core'

### Incompatible Changes 
- Moved IGrpcChannelProvider & GrpcChannelProvider to SiLA2.Utils
- Created IGrpcServerChannelProvider & GrpcServerChannelProvider for serverside channel creation in SiLA2
- Deleted NetworkService & renamed INetworker & Networker to NetworkService in SiLA2.Utils

### Compatible Changes
- Included Binary-Upload and -Download implementation in SiLA2
- NetworkService.GetNetworkInterfaceByName looks for an active interface by name. If it is not found it will take the first active NetworkInterface or throw an NetworkInterfaceNotFoundException in SiLA2.Utils


## [6.0.0] - (everything before introduction of the changelog)
- First Release with SiLA2.Server.nupkg and SiLA2.Utils.nupkg on nuget.org
