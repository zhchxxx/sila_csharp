﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using SiLA2.Utils;

namespace SiLA2.Server.Interceptors
{
    public class MetadataValidationInterceptor : Interceptor
    {
        private readonly ILogger _logger;
        private readonly ISiLA2Server _server;

        public MetadataValidationInterceptor(ILogger<MetadataValidationInterceptor> logger, ISiLA2Server server)
        {
            _logger = logger;
            _server = server;
        }

        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            // get contained metadata identifiers from context
            var containedMetadata = SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders);
            foreach (var metadata in _server.MetadataManager.GetRequiredMetadataForCall(context.Method))
            {
                if (!containedMetadata.Contains(metadata.Key))
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, $"No metadata '{metadata.Key}' found in client request"));
                }
            }

            return await continuation(request, context);
        }

        public override async Task ServerStreamingServerHandler<TRequest, TResponse>(TRequest request, IServerStreamWriter<TResponse> responseStream, ServerCallContext context, ServerStreamingServerMethod<TRequest, TResponse> continuation)
        {
            // only check metadata for "Subscribe_" calls
            if (Regex.Match(context.Method, "\\/sila2\\..*Subscribe_[A-Z][a-zA-Z0-9]*$").Success)
            {
                // get contained metadata identifiers from context
                var containedMetadata = SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders);
                foreach (var metadata in _server.MetadataManager.GetRequiredMetadataForCall(context.Method))
                {
                    if (!containedMetadata.Contains(metadata.Key))
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, $"No metadata '{metadata.Key}' found in client request"));
                    }
                }
            }

            await continuation(request, responseStream, context);
        }
    }
}
