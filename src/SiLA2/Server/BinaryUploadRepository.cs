﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public class BinaryUploadRepository : IBinaryUploadRepository
    {
        public ConcurrentDictionary<Guid, IBinaryUploadRepository.UploadData> UploadDataMap { get; } = new();

        public async Task<Guid> CheckBinaryUploadTransferUUID(string binaryTransferUUIDString)
        {
            return await Task.Run(() =>
            {
                if (!Guid.TryParse(binaryTransferUUIDString, out var binaryTransferUUID) || !UploadDataMap.ContainsKey(binaryTransferUUID))
                {
                    throw new Exception($"The given Binary Upload Transfer UUID '{binaryTransferUUIDString}' is not valid");
                }

                return binaryTransferUUID;
            });
        }

        public async Task<byte[]> GetUploadedData(string binaryTransferUUIDString)
        {
            return await Task.Run(() =>
            {
                if (!Guid.TryParse(binaryTransferUUIDString, out var binaryTransferUUID))
                {
                    throw new Exception($"The given Binary Transfer UUID '{binaryTransferUUIDString}' is not valid");
                }

                if (!UploadDataMap.ContainsKey(binaryTransferUUID))
                {
                    throw new Exception($"No uploaded data found for the given Binary Transfer UUID '{binaryTransferUUID}' ");
                }

                if (UploadDataMap[binaryTransferUUID].Data.Any(x => x == null))
                {
                    throw new Exception($"Not all chunks have been uploaded for Binary Transfer UUID '{binaryTransferUUID}'");
                }

                var uploadSize = UploadDataMap[binaryTransferUUID].Data.Sum(x => x.Length);
                var uploadData = new byte[uploadSize];

                int pos = 0;
                for (var i = 0; i < UploadDataMap[binaryTransferUUID].Data.Length; i++)
                {
                    Array.Copy(UploadDataMap[binaryTransferUUID].Data[i], 0, uploadData, pos, UploadDataMap[binaryTransferUUID].Data[i].Length);
                    pos += UploadDataMap[binaryTransferUUID].Data[i].Length;
                }

                return uploadData;
            });
        }

        public async Task<ulong> GetUploadedDataSize(Guid binaryTransferUUID)
        {
            return await Task.Run(() =>
            {
                ulong result = 0;

                if (!UploadDataMap.ContainsKey(binaryTransferUUID))
                {
                    throw new Exception($"No uploaded data found for the given Binary Transfer UUID '{binaryTransferUUID}' ");
                }

                foreach (var chunk in UploadDataMap[binaryTransferUUID].Data)
                {
                    if (chunk != null)
                    {
                        result += (ulong)chunk.Length;
                    }
                }

                return result;
            });
        }
    }
}