﻿using Sila2.Org.Silastandard.Core.Authenticationservice.V1;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public interface IAuthenticationInspector
    {
        Task<bool>IsAuthenticated(Login_Parameters login);
    }
}
