﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Core.Authorizationservice.V1;
using SiLA2.Server.Utils;
using SiLA2.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Authorizationservice.V1.AuthorizationService;
using GrpcCoreMetaData = Grpc.Core.Metadata;

namespace SiLA2.Server.Services
{
    public class AuthorizationService : AuthorizationServiceBase//, IAuthorizationService
    {
        private const string FullyQualifiedFeatureIdentifier = "org.silastandard/core/AuthorizationService/v1";

        public Feature SilaFeature { get; }

        private readonly ISiLA2Server _silaServer;

        /// <summary>
        /// Contains the fully qualified identifiers of Features,
        /// Commands and Properties that are affected by access protection.
        /// </summary>
        private readonly List<string> _accessProtectedRequests = new();

        public AuthorizationService(ISiLA2Server silaServer)
        {
            _silaServer = silaServer;
            SilaFeature = _silaServer.GetFeature(FullyQualifiedFeatureIdentifier) ?? _silaServer.ReadFeature(Path.Combine("Features", "AuthorizationService-v1_0.sila.xml"));
        }

        /// <summary>
        /// Adds fully qualified identifiers of Features, Commands
        /// and/or Properties that shall be access protected.
        /// </summary>
        public void AddAccessProtectedItems(List<string> items)
        {
            _accessProtectedRequests.AddRange(items);
            _silaServer.MetadataManager.CollectMetadataAffections(SilaFeature, this);
        }

        public bool IsAuthorized(Guid token)
        {
            return AuthorizationTokenRepository.Instance.IsTokenValid(token);
        }

        public bool IsAuthorized(GrpcCoreMetaData metaData)
        {
            var silaMetaDataKey = SilaClientMetadata.ConvertMetadataIdentifierToWireFormat(SilaFeature.GetFullyQualifiedMetadataIdentifier("AccessToken"));
            var tokenBytes = metaData.GetValueBytes(silaMetaDataKey);
            if (tokenBytes == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "AccessToken metadata value could not be parsed by identifier (wrong message type)"));
            }

            var tokenMetaData = Metadata_AccessToken.Parser.ParseFrom(tokenBytes);
            if (tokenMetaData.AccessToken?.Value == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "AccessToken metadata value could not be parsed (wrong message type)"));
                return false;
            }

            if (!Guid.TryParse(tokenMetaData.AccessToken.Value, out var accessToken) || !AuthorizationTokenRepository.Instance.IsTokenValid(accessToken))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SilaFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier("InvalidAccessToken"), "AccessToken is not valid"));
            }

            return true;
        }

        public override Task<Get_FCPAffectedByMetadata_AccessToken_Responses> Get_FCPAffectedByMetadata_AccessToken(Get_FCPAffectedByMetadata_AccessToken_Parameters request, ServerCallContext context)
        {
            Get_FCPAffectedByMetadata_AccessToken_Responses response = new();
            foreach (var item in _accessProtectedRequests)
            {
                response.AffectedCalls.Add(new Sila2.Org.Silastandard.String { Value = item });
            }

            return Task.FromResult(response);
        }
    }
}
