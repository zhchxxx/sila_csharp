﻿using System.Text.RegularExpressions;

namespace SiLA2.Server.Utils
{
    /// <summary>
    /// Provides utilities for working with SiLA Fully Qualified Identifiers
    /// </summary>
    internal class FullyQualifiedIdentifierUtils
    {
        private const string OriginatorRegex = "[a-z][a-z.]*";
        private const string CategoryRegex = "[a-z][a-z.]*";
        private const string IdentifierRegex = "[A-Z][a-zA-Z0-9]*";
        private const string MajorVersionRegex = "v\\d+";

        private static readonly string FullyQualifiedFeatureIdentifierRegex = string.Join("/", OriginatorRegex, CategoryRegex, IdentifierRegex, MajorVersionRegex);
        private static readonly string FullyQualifiedCommandIdentifierRegex = string.Join("/", FullyQualifiedFeatureIdentifierRegex, "Command", IdentifierRegex);

        /// <summary>Check if given identifier is a fully qualified SiLA 2 identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for any fully qualified identifier</returns>
        public static bool IsFullyQualifiedIdentifier(string identifier) =>
            IsFullyQualifiedFeatureIdentifier(identifier) ||
            IsFullyQualifiedCommandIdentifier(identifier) ||
            IsFullyQualifiedCommandParameterIdentifier(identifier) ||
            IsFullyQualifiedCommandResponseIdentifier(identifier) ||
            IsFullyQualifiedIntermediateCommandResponseIdentifier(identifier) ||
            IsFullyQualifiedDefinedExecutionErrorIdentifier(identifier) ||
            IsFullyQualifiedPropertyIdentifier(identifier) ||
            IsFullyQualifiedCustomDataTypeIdentifier(identifier) ||
            IsFullyQualifiedMetadataIdentifier(identifier);

        /// <summary>Check if given identifier is a fully qualified feature identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified feature identifier</returns>
        public static bool IsFullyQualifiedFeatureIdentifier(string identifier) => Regex.Match(identifier, $"{FullyQualifiedFeatureIdentifierRegex}$").Success;

        /// <summary>Check if given identifier is a fully qualified command identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified command identifier</returns>
        public static bool IsFullyQualifiedCommandIdentifier(string identifier) => Regex.Match(identifier, $"{FullyQualifiedCommandIdentifierRegex}$").Success;

        /// <summary>Check if given identifier is a fully qualified command parameter identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified command parameter identifier</returns>
        public static bool IsFullyQualifiedCommandParameterIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedCommandIdentifierRegex, "Parameter", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified intermediate response identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified command response identifier</returns>
        public static bool IsFullyQualifiedCommandResponseIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedCommandIdentifierRegex, "Response", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified intermediate command response identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified intermediate command response identifier</returns>
        public static bool IsFullyQualifiedIntermediateCommandResponseIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedCommandIdentifierRegex, "IntermediateResponse", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified defined execution error identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified defined execution error identifier</returns>
        public static bool IsFullyQualifiedDefinedExecutionErrorIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedFeatureIdentifierRegex, "DefinedExecutionError", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified property identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified property identifier</returns>
        public static bool IsFullyQualifiedPropertyIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedFeatureIdentifierRegex, "Property", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified custom data type identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified custom data type identifier</returns>
        public static bool IsFullyQualifiedCustomDataTypeIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedFeatureIdentifierRegex, "DataType", IdentifierRegex)}$").Success;

        /// <summary>Check if given identifier is a fully qualified metadata identifier</summary>
        /// <param name="identifier">The identifier to be checked</param>
        /// <returns>True if given identifier matches the rules for a fully qualified metadata identifier</returns>
        public static bool IsFullyQualifiedMetadataIdentifier(string identifier) => Regex.Match(identifier, $"{string.Join("/", FullyQualifiedFeatureIdentifierRegex, "Metadata", IdentifierRegex)}$").Success;
    }
}