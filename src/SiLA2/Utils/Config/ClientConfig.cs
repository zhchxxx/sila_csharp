﻿using Microsoft.Extensions.Configuration;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SiLA2.Utils.Config
{
    public class ClientConfig : IClientConfig, IEquatable<ClientConfig>
    {

        [DataMember(IsRequired = true)]
        public string IpOrCdirOrFullyQualifiedHostName { get; set; }

        [DataMember(IsRequired = true)]
        public int Port { get; set; }

        public string ClientName { get; set; }

        public string NetworkInterface { get; set; }

        public string DiscoveryServiceName { get; set; }

        public ClientConfig(IConfiguration configuration) : this()
        {
            IpOrCdirOrFullyQualifiedHostName = configuration["Connection:FQHN"];
            Port = int.Parse(configuration["Connection:Port"]);
            NetworkInterface = configuration["Connection:ServerDiscovery:NIC"];
            DiscoveryServiceName = configuration["Connection:ServerDiscovery:ServiceName"];
        }

        public ClientConfig(string ipOrCdirOrFullyQualifiedHostName, int port, string networkInterface, string discoveryServiceName) : this()
        {
            IpOrCdirOrFullyQualifiedHostName = ipOrCdirOrFullyQualifiedHostName;
            Port = port;
            NetworkInterface = networkInterface;
            DiscoveryServiceName = discoveryServiceName;
        }

        public ClientConfig(string clientName, string ipOrCdirOrFullyQualifiedHostName, int port)
        {
            ClientName = clientName;
            IpOrCdirOrFullyQualifiedHostName = ipOrCdirOrFullyQualifiedHostName;
            Port = port;
        }

        //Nedded for deserialization
        public ClientConfig() 
        {
            ClientName = ToString();
        }

        public override string ToString()
        {
            return $"{IpOrCdirOrFullyQualifiedHostName}:{Port}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ClientConfig);
        }

        public bool Equals(ClientConfig other)
        {
            return other is not null &&
                   IpOrCdirOrFullyQualifiedHostName == other.IpOrCdirOrFullyQualifiedHostName &&
                   Port == other.Port &&
                   ClientName == other.ClientName;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(IpOrCdirOrFullyQualifiedHostName, Port, ClientName);
        }

        public static bool operator ==(ClientConfig left, ClientConfig right)
        {
            return EqualityComparer<ClientConfig>.Default.Equals(left, right);
        }

        public static bool operator !=(ClientConfig left, ClientConfig right)
        {
            return !(left == right);
        }
    }
}
