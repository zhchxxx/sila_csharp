﻿using SiLA2.Database.SQL.Domain;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Database.SQL
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> GetById(object id);
        Task<TransactionResultMessage> Insert(T entity);
        Task<TransactionResultMessage> Update(T entity);
        Task<TransactionResultMessage> Delete(T entity);
        IQueryable<T> Table { get; }
    }
}