﻿using System.Reflection;

namespace SiLA2.Frontend.Razor.ViewModels
{
    public class PropertyInfoViewModel
    {
        public PropertyInfo PropertyInfo { get; }
        public string PropertyValue { get; set; } 

        public PropertyInfoViewModel(PropertyInfo propertyInfo)
        {
            PropertyInfo = propertyInfo;
        }

        public override string ToString()
        {
            return $"{PropertyInfo.Name} ({PropertyInfo.PropertyType})";
        }
    }
}
