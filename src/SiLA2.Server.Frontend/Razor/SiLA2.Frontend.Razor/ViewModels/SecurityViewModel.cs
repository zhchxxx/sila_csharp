﻿using Microsoft.AspNetCore.Components.Forms;
using SiLA2.Utils.Security;
using System.Collections.Generic;

namespace SiLA2.Frontend.Razor.ViewModels
{
    internal class SecurityViewModel
    {
        public IEnumerable<string> CertificateItemNames => new string[] { Constants.SERVER_CRT_FILENAME, Constants.SERVER_KEY_FILENAME, Constants.CA_CRT_FILENAME, Constants.CA_KEY_FILENAME };
        public string SelectedCertificateItemName { get; set; }
        public IDictionary<string, IBrowserFile> CertificateItemMap { get; set; } = new Dictionary<string, IBrowserFile>() { { Constants.SERVER_CRT_FILENAME, null }, { Constants.SERVER_KEY_FILENAME, null }, { Constants.CA_CRT_FILENAME, null }, { Constants.CA_KEY_FILENAME, null } };

        public SecurityViewModel()
        {
            SelectedCertificateItemName = Constants.CA_CRT_FILENAME;
        }
    }
}
