﻿using Grpc.Core;
using SiLA2.Frontend.Razor.ViewModels;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services
{
    public interface IGrpcClientProvider
    {
        Dictionary<string, ClientBase> ClientMap { get; }
        Task CreateClients();
        Task CreateGrpcClient(string ns);
        Task<IEnumerable<MethodInfo>> GetMethodInfosAsync(string ns);
        IEnumerable<MethodInfo> GetMethodInfos(string ns);
        Task InvokeMethod(string featureTypeIdentifier, FeatureMethodInfoViewModel methodResultBundle);
    }
}