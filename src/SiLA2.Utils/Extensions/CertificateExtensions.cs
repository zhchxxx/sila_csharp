﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace SiLA2.Utils.Extensions
{
    public static class CertificateExtensions
    {
        public static X509Certificate2 CompensateWindowsEpheralKeysProblem(this X509Certificate2 cert)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                // SSLStream on Windows throws with ephemeral key sets
                // workaround from https://github.com/dotnet/runtime/issues/23749#issuecomment-388231655
                var originalCert = cert;
                cert = new X509Certificate2(cert.Export(X509ContentType.Pkcs12));
                originalCert.Dispose();
            }
            return cert;
        }

        public static byte[] ExtractFromPem(this string pem)
        {
            var data = PemEncoding.Find(pem);
            return Convert.FromBase64String(pem[data.Base64Data]);
        }
    }
}
