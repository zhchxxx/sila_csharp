﻿namespace SiLA2.Utils.Extensions
{
    /// <summary>
    /// Provides methods to convert values between several units (to be extended on demand).
    /// </summary>
    public static class UnitConverterExtensions
    {
        #region Temperature

        public static double Kelvin2DegreeCelsius(this double value) { return value - 273.15; }

        public static double DegreeCelsius2Kelvin(this double value) { return value + 273.15; }

        public static double Kelvin2DegreeFahrenheit(this double value) { return value * 9 / 5 - 459.67; }

        public static double DegreeFahrenheit2Kelvin(this double value) { return (value + 459.67) * 5 / 9; }

        #endregion
    }
}