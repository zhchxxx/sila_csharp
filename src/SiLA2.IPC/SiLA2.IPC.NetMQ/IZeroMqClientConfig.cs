﻿using System;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqClientConfig
    {
        string ServerSocketAddress { get; }
        bool UseSingleFrame { get; set; }
        bool ExpectServerResponse { get; set; }
        TimeSpan Timeout { get; }
    }
}