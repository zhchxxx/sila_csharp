﻿using Microsoft.Extensions.Logging;
using NetMQ;
using NetMQ.Sockets;

namespace SiLA2.IPC.NetMQ
{
    public class ZeroMqClient : IZeroMqClient
    {
        private readonly ILogger<ZeroMqClient> _logger;

        public IZeroMqClientConfig ZeroMqClientConfig { get; }

        public ZeroMqClient(IZeroMqClientConfig zeroMqClientConfig, ILogger<ZeroMqClient> logger)
        {
            ZeroMqClientConfig = zeroMqClientConfig;
            _logger = logger;
        }

        public string Send(string message)
        {
            using (var client = new RequestSocket())
            {
                client.Connect(ZeroMqClientConfig.ServerSocketAddress);
                _logger.LogDebug($"Sending message from Client : '{message}'");
                client.SendFrame(message);

                if (ZeroMqClientConfig.ExpectServerResponse)
                {
                    return ReturnServerMessage(message, client);
                }
            }
            return string.Empty;
        }

        public string Send(NetMQMessage netMQMessage)
        {
            using (var client = new RequestSocket())
            {
                client.Connect(ZeroMqClientConfig.ServerSocketAddress);
                _logger.LogDebug($"Sending NetMQMessage from Client : '{netMQMessage}'");
                client.SendMultipartMessage(netMQMessage);

                if (ZeroMqClientConfig.ExpectServerResponse)
                {
                    return ReturnServerMessage(netMQMessage.ToString(), client);
                }
            }
            return string.Empty;
        }

        private string ReturnServerMessage(string message, RequestSocket client)
        {
            var success = client.TryReceiveFrameString(ZeroMqClientConfig.Timeout, out string serverResponse);

            if (success)
            {
                _logger.LogDebug($"Server Response : '{serverResponse}'");
                return serverResponse;
            }
            else
            {
                var failMsg = $"Failed to receive Server Response for Message '{message}'";
                _logger.LogError(failMsg);
                throw new NetMQException(failMsg);
            }
        }
    }
}