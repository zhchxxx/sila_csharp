﻿using NetMQ;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqClient
    {
        IZeroMqClientConfig ZeroMqClientConfig { get; }

        string Send(string message);

        string Send(NetMQMessage netMQMessage);
    }
}