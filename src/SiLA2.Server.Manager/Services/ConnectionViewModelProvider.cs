﻿using SiLA2.Server.Manager.Data;
using System.Collections.Concurrent;

namespace SiLA2.Server.Manager.Services
{
    public class ConnectionViewModelProvider : IConnectionViewModelProvider
    {
        public ConcurrentDictionary<string, ConnectionViewModel> Servers { get; } = new ConcurrentDictionary<string, ConnectionViewModel>();
    }
}
