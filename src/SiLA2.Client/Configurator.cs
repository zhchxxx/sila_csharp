﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Sila2.Utils;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class Configurator : IConfigurator
    {
        private ILogger<Configurator> _logger;

        public IServiceCollection Container { get; private set; } = new ServiceCollection();

        public IServiceProvider ServiceProvider { get; private set; }

        public IDictionary<Guid, ServerData> DiscoveredServers { get; private set; } = new Dictionary<Guid, ServerData>();

        public Configurator(IConfiguration configuration, string[] args)
        {
            SetupContainer(configuration);
            ServiceProvider = Container.BuildServiceProvider();
            var serverConfig = ServiceProvider.GetService<IClientConfig>();
            args.ParseClientCmdLineArgs<CmdLineClientArgs>(serverConfig);
            _logger = ServiceProvider.GetService<ILogger<Configurator>>();
        }

        public async Task<IDictionary<Guid, ServerData>> SearchForServers()
        {
            var servers = await ServiceProvider.GetService<ISiLAServerDiscovery>().GetServers();

            foreach (var server in servers)
            {
                if (!DiscoveredServers.ContainsKey(server.Config.Uuid))
                {
                    DiscoveredServers.Add(server.Config.Uuid, server);
                }
                else
                {
                    DiscoveredServers[server.Config.Uuid] = server;
                }
                _logger.LogInformation($"Found {server.Config}");
            }

            return DiscoveredServers;
        }

        private void SetupContainer(IConfiguration configuration)
        {
            Container.AddLogging();
            Container.AddSingleton<IServiceFinder, ServiceFinder>();
            Container.AddSingleton<INetworkService, NetworkService>();
            Container.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            Container.AddSingleton<ISiLAServerDiscovery, SiLAServerDiscovery>();
            Container.AddScoped<IServerDataProvider, ServerDataProvider>();
            Container.AddSingleton<IClientConfig>(new ClientConfig(configuration));
            Container.AddSingleton<IServerConfig, ServerConfig>();
        }

        public async Task<GrpcChannel> GetChannel(bool acceptAnyServerCertificate = true)
        {
            var clientConfig = ServiceProvider.GetService<IClientConfig>();
            return await ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, acceptAnyServerCertificate);
        }

        public async Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = true)
        {
            return await ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(host, port, acceptAnyServerCertificate);
        }


        public void UpdateServiceProvider()
        {
            ServiceProvider = Container.BuildServiceProvider();
        }
    }
}