﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiLA2.Communication.Services
{

    /* 
     * TODOs: Create Protobuf-net-Messages & use them in gRPC-Services dependent on SiLA2 Feature
     *          - Message Factory (recursive DataTypeType conversion)
     *              - Input: specific SiLA Feature
     *          - gRPC-Service Factory (TecanSDK uses specific SilaChannel)
     *              - Input: specific SiLA Feature
     *          - Dynamic Client executes gRPC-Call with messages created at runtime
     *              - Features can be retrieved by calling SiLAService.GetImplementedFeatures() of SiLA Server
     */

    public class PayloadFactory : IPayloadFactory
    {
    }

    public interface IPayloadFactory
    {

    }
}
