﻿using ProtoBuf;
using System.Reflection.Emit;
using System.Reflection;

namespace SiLA2.Communication.Protobuf
{
    public class DynamicProtobufTypeBuilder
    {
        public Type Build(string typeName, IDictionary<string, Type> properties, string dynAsmName = "Dynamic.Protobuf.Assembly")
        {
            AssemblyName assemblyName = new AssemblyName(dynAsmName);
            AssemblyBuilder assemblyBuilder =
                AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("DynamicProtobufModule");
            TypeBuilder typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.Public);

            // Add ProtoContract attribute to the class
            Type protoContractAttributeType = typeof(ProtoContractAttribute);
            ConstructorInfo protoContractAttributeCtor =
                protoContractAttributeType.GetConstructor(new Type[] { });
            CustomAttributeBuilder protoContractAttributeBuilder =
                new CustomAttributeBuilder(protoContractAttributeCtor, new object[] { });
            typeBuilder.SetCustomAttribute(protoContractAttributeBuilder);

            int protobufPropertyPosition = 1;
            foreach (var propertyBundle in properties)
            {
                // Create a field
                FieldBuilder field = typeBuilder.DefineField($"_{propertyBundle.Key}", propertyBundle.Value, FieldAttributes.Private);

                // Create a property
                PropertyBuilder property = typeBuilder.DefineProperty(propertyBundle.Key, PropertyAttributes.HasDefault, propertyBundle.Value, null);

                // Create a property getter
                MethodBuilder getter = typeBuilder.DefineMethod($"get_{propertyBundle.Key}", MethodAttributes.Public | MethodAttributes.SpecialName, propertyBundle.Value, Type.EmptyTypes);
                ILGenerator getterIL = getter.GetILGenerator();
                getterIL.Emit(OpCodes.Ldarg_0);
                getterIL.Emit(OpCodes.Ldfld, field);
                getterIL.Emit(OpCodes.Ret);
                property.SetGetMethod(getter);

                // Create a property setter
                MethodBuilder setter = typeBuilder.DefineMethod($"set_{propertyBundle.Key}", MethodAttributes.Public | MethodAttributes.SpecialName, null, new Type[] { propertyBundle.Value });
                ILGenerator setterIL = setter.GetILGenerator();
                setterIL.Emit(OpCodes.Ldarg_0);
                setterIL.Emit(OpCodes.Ldarg_1);
                setterIL.Emit(OpCodes.Stfld, field);
                setterIL.Emit(OpCodes.Ret);
                property.SetSetMethod(setter);

                // Add ProtoMember attribute to the property with order number
                Type protoMemberAttributeType = typeof(ProtoMemberAttribute);
                ConstructorInfo protoMemberAttributeCtor = protoMemberAttributeType.GetConstructor(new Type[] { typeof(int) });
                CustomAttributeBuilder protoMemberAttributeBuilder = new CustomAttributeBuilder(protoMemberAttributeCtor, new object[] { protobufPropertyPosition });
                property.SetCustomAttribute(protoMemberAttributeBuilder);

                protobufPropertyPosition++;
            }

            // Create the class
            return typeBuilder.CreateType();
        }
    }

}
