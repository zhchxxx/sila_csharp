﻿using ProtoBuf;

namespace SiLA2.Communication.Protobuf
{
    public static class ByteSerializer
    {
        public static T FromByteArray<T>(byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                return Serializer.Deserialize<T>(ms);
            }
        }

        public static byte[] ToByteArray<T>(T instance)
        {
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, instance);
                var data = ms.ToArray();
                return data;
            }
        }

        public static ByteSerializer<T> GetDefault<T>()
        {
            return new ByteSerializer<T>(ToByteArray, FromByteArray<T>);
        }
    }

    public readonly struct ByteSerializer<T>
    {
        public Func<T, byte[]> Serializer { get; }

        public Func<byte[], T> Deserializer { get; }

        public ByteSerializer(Func<T, byte[]> serializer, Func<byte[], T> deserializer)
        {
            Serializer = serializer;
            Deserializer = deserializer;
        }
    }
}