﻿using AnIMLCore;
using LiteDB;
using SiLA2.Database.NoSQL;
using System.Text;
using System.Xml.Serialization;

namespace SiLA2.AnIML.Services
{
    public class AnIMLRepository : BaseRepository<AnIMLType>, IAnIMLRepository
    {
        public AnIMLRepository(ILiteDatabase db) : base(db)
        {
        }
    }
}