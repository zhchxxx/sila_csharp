﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2;
using SiLA2.Client;
using SiLA2.Domain;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLAFramework = Sila2.Org.Silastandard;

var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
var configuration = configBuilder.Build();

var clientSetup = new Configurator(configuration, args);
ILogger<Program> logger = (ILogger<Program>)clientSetup.ServiceProvider.GetService(typeof(ILogger<Program>));

var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
GrpcChannel channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);

Console.WriteLine("\n");

var testCount = 0;
var testsPassed = 0;

// AnyDataTypesTest
//--------------------
const int ExpectedResultOf_Get_AnyTypeIntegerValue = 5124;

var anyTypeTestClient = new SiLAFramework.Test.Anytypetest.V1.AnyTypeTest.AnyTypeTestClient(channel);

// Integer value
var anyTypeResult = anyTypeTestClient.Get_AnyTypeIntegerValue(new SiLAFramework.Test.Anytypetest.V1.Get_AnyTypeIntegerValue_Parameters());
var dataType = AnyType.ExtractAnyType(anyTypeResult.AnyTypeIntegerValue.Type);

testCount++;
if (dataType.Item is BasicType && (BasicType)dataType.Item == BasicType.Integer) { testsPassed++; }
else { logger.LogError($"[Get_AnyTypeIntegerValue] Wrong any type data type: {dataType}"); }

testCount++;
var integerValue = (int)SiLAFramework.Integer.Parser.ParseFrom(anyTypeResult.AnyTypeIntegerValue.Payload).Value;
if (integerValue == ExpectedResultOf_Get_AnyTypeIntegerValue) { testsPassed++; }
else { logger.LogError($"[Get_AnyTypeIntegerValue] Wrong integer value: {integerValue}"); }


// BasicDataTypesTest
//--------------------
const int echoedIntegerValue = 42;

var basicDataTypesTestClient = new SiLAFramework.Test.Basicdatatypestest.V1.BasicDataTypesTest.BasicDataTypesTestClient(channel);

testCount++;
var intResult = basicDataTypesTestClient.EchoIntegerValue(new SiLAFramework.Test.Basicdatatypestest.V1.EchoIntegerValue_Parameters { IntegerValue = new SiLAFramework.Integer { Value = echoedIntegerValue } });
if (intResult.ReceivedValue.Value == echoedIntegerValue) { testsPassed++; }
else { logger.LogError($"[EchoIntegerValue] Wrong integer value: {intResult.ReceivedValue.Value}"); }


// ErrorHandlingTest
//--------------------
var errorHandlingTestClient = new SiLAFramework.Test.Errorhandlingtest.V1.ErrorHandlingTest.ErrorHandlingTestClient(channel);
try
{
    errorHandlingTestClient.RaiseDefinedExecutionError(new SiLAFramework.Test.Errorhandlingtest.V1.RaiseDefinedExecutionError_Parameters());
}
catch (Exception ex)
{
    logger.LogInformation("Received error: " + SiLA2.Server.Utils.ErrorHandling.HandleException(ex));
}

// ParameterConstraintsTest
//--------------------------
testCount++;
var parameterConstraintsTestClient = new SiLAFramework.Test.Parameterconstraintstest.V1.ParameterConstraintsTest.ParameterConstraintsTestClient(channel);
try
{
    parameterConstraintsTestClient.CheckStringConstraintLength(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "0123456789" } });
    parameterConstraintsTestClient.CheckStringConstraintMinimalLength(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintMinimalLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "big enough" } });
    parameterConstraintsTestClient.CheckStringConstraintMaximalLength(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintMaximalLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "short enough" } });
    parameterConstraintsTestClient.CheckStringConstraintMinMaxLength(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintMinMaxLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "fitting constraints" } });
    parameterConstraintsTestClient.CheckStringConstraintSet(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "Second option" } });
    parameterConstraintsTestClient.CheckStringConstraintPattern(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintPattern_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "08/05/2018" } });
    //parameterConstraintsTestClient.CheckStringConstraintFullyQualifiedIdentifier(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckStringConstraintFullyQualifiedIdentifier_Parameters
    //{
    //    FeatureIdentifier = new SiLAFramework.String { Value = "hallo ballo" }
    //});

    parameterConstraintsTestClient.CheckIntegerConstraintSet(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 3 } });
    parameterConstraintsTestClient.CheckScientificallyNotatedIntegerLimit(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckScientificallyNotatedIntegerLimit_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = (long)-1e2 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMaximalExclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMaximalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMaximalInclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMaximalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMinimalExclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMinimalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMinimalInclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMinimalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMinMax(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } });
    parameterConstraintsTestClient.CheckIntegerConstraintMinMax(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } });
    parameterConstraintsTestClient.CheckIntegerConstraintUnit(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckIntegerConstraintUnit_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 123 } });

    parameterConstraintsTestClient.CheckRealConstraintSet(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 2.22 } });
    parameterConstraintsTestClient.CheckScientificallyNotatedRealLimit(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckScientificallyNotatedRealLimit_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1e-2 } });
    parameterConstraintsTestClient.CheckRealConstraintMaximalExclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintMaximalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = -5e3 } });
    parameterConstraintsTestClient.CheckRealConstraintMaximalInclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintMaximalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 10 } });
    parameterConstraintsTestClient.CheckRealConstraintMinimalExclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintMinimalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1e-5 } });
    parameterConstraintsTestClient.CheckRealConstraintMinimalInclusive(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintMinimalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 0 } });
    parameterConstraintsTestClient.CheckRealConstraintMinMax(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1.23 } });
    parameterConstraintsTestClient.CheckRealConstraintUnit(new SiLAFramework.Test.Parameterconstraintstest.V1.CheckRealConstraintUnit_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 123 } });
}
catch (Exception ex)
{
    logger.LogInformation("Received error: " + SiLA2.Server.Utils.ErrorHandling.HandleException(ex));
    testsPassed--;
}

testsPassed++;


Thread.Sleep(1000);
Console.WriteLine($"\n{testsPassed} of {testCount} tests passed\n\nPress any key to exit...");
Console.ReadKey();