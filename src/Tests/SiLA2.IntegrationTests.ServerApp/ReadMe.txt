﻿
Info
====

- all certificates and keys are pem encoded
- server.crt & server.key were created by SiLA2.Python

		from sila2.server.encryption import generate_self_signed_certificate
		import uuid

		server_uuid = uuid.uuid4()
		key, cert = generate_self_signed_certificate(server_uuid, "localhost")

		open("key.pem", "wb").write(key)
		open("cert.pem", "wb").write(cert)
		open("uuid.txt", "wt").write(str(server_uuid))


- ca.cert is equal to server.cert & ca.key ist equal to server.key
- Implementation of deriving server.crt from Root Certificate is missing...