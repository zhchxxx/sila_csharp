﻿using NUnit.Framework;
using ProtoBuf;
using SiLA2.Communication.Protobuf;
using SiLA2.IntegrationTests.ServerApp;
using System;
using System.Linq;
using System.Collections.Generic;
using ProtoBuf.Grpc.Client;
using Grpc.Core;
using UnitTest.Utils;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    internal class DynamicClientTests
    {
        TestServerFixture<Program> _testServerFixture;
        private Feature _silaServiceFeature;
        private const string EXPECTED_SERVER_UUID = "A82121B1-22DE-4B81-A450-86FA2E5344EE";
        private const string EXPECTED_SERVER_NAME = "SiLA2 Integration Test Server";
        private const string EXPECTED_SERVER_TYPE = "SiLA2IntegrationTestServer";
        private const string EXPECTED_SERVER_DESCRIPTION = "SiLA2 Integration Test Server. Cross-platform implementation for .NET with web server Kestrel as Service Host.";
        private const string EXPECTED_SERVER_VENDOR_URL = "https://sila2.gitlab.io/sila_base/";
        private const string EXPECTED_SERVER_VERSION = "7.4.5";

        [ProtoContract]
        public class Response
        {
            [ProtoMember(1)]
            public Sila2.Org.Silastandard.Protobuf.String ServerUUID { get; set; }
        }

        [OneTimeSetUp]
        public void SetupOnce()
        {
            // System under Test
            var args = new string[] { };
            _testServerFixture = new TestServerFixture<Program>(args);
            _silaServiceFeature = ((ISiLA2Server)_testServerFixture.ServiceProvider.GetService(typeof(ISiLA2Server))).ReadFeature("SiLAService-v1_0.sila.xml");
        }

        [Test]
        public void Should_Get_ServerUUID()
        {
            //Arrange
            var grpcService = $"{_silaServiceFeature.Namespace}.{_silaServiceFeature.Identifier}"; //"sila2.org.silastandard.core.silaservice.v1.SiLAService"
            var featureProperties = _silaServiceFeature.GetDefinedProperties();
            var featurePropertyUuid = $"Get_{featureProperties.Single(x => x.Identifier.Equals("ServerUUID")).Identifier}";

            // Systen under Test
            var method = new Method<EmptyMessage, Response>(
                                    MethodType.Unary,
                                    grpcService,
                                    featurePropertyUuid,
                                    ProtobufMarshaller<EmptyMessage>.Default,
                                    ProtobufMarshaller<Response>.Default);

            // Act
            var response = _testServerFixture.GrpcChannel.CreateCallInvoker().BlockingUnaryCall(method, null, new CallOptions(), new EmptyMessage());

            // Assert
            Assert.That(response.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Invoke_Method_Protobuf_Grpc_Client()
        {
            // Arrange
            var grpcService = $"{_silaServiceFeature.Namespace}.{_silaServiceFeature.Identifier}";
            var featureProperties = _silaServiceFeature.GetDefinedProperties();
            var operationName = $"Get_{featureProperties.Single(x => x.Identifier.Equals("ServerUUID")).Identifier}";

            // System under Test
            var client = new GrpcClient(_testServerFixture.GrpcChannel.CreateCallInvoker(), grpcService);

            // Act
            var response = client.BlockingUnary<EmptyMessage, Response>(new EmptyMessage(), operationName);

            // Assert
            Assert.That(response.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Create_And_Invoke_Grpc_Request_At_Runtime()
        {
            // Arrange
            var grpcService = $"{_silaServiceFeature.Namespace}.{_silaServiceFeature.Identifier}";
            var featureProperties = _silaServiceFeature.GetDefinedProperties();
            var operationName = $"Get_{featureProperties.Single(x => x.Identifier.Equals("ServerUUID")).Identifier}";

            var method = typeof(GrpcClient).GetMethods().Where(x => x.Name.StartsWith("BlockingUnary")).First();// ("BlockingUnary").MakeGenericMethod(typeof(EmptyMessage), typeof(Response));
            var genericMethod = method.MakeGenericMethod(typeof(EmptyMessage), typeof(Response));

            // System under Test
            var client = new GrpcClient(_testServerFixture.GrpcChannel.CreateCallInvoker(), grpcService);

            // Act
            dynamic response = genericMethod.Invoke(client, new object[] { new EmptyMessage(), operationName, null });

            // Assert
            Assert.That(response.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Create_Dynamic_ProtbufClass_At_Runtime()
        {
            // Arrange
            var propertyMap = new Dictionary<string, Type>() { { "ServerUUID", typeof(Sila2.Org.Silastandard.Protobuf.String) } };

            // System under Test
            var dynamicProtobufBuilder = new DynamicProtobufTypeBuilder();

            // Act
            var dynProtoType = dynamicProtobufBuilder.Build("DynamicProtobufTestClass", propertyMap);
            dynamic protoObj = Activator.CreateInstance(dynProtoType);
            protoObj.ServerUUID = new Sila2.Org.Silastandard.Protobuf.String { Value = EXPECTED_SERVER_UUID };

            // Assert
            Assert.That(protoObj.ServerUUID.Value, Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [TestCase("ServerUUID", EXPECTED_SERVER_UUID)]
        [TestCase("ServerName", EXPECTED_SERVER_NAME)]
        [TestCase("ServerType", EXPECTED_SERVER_TYPE)]
        [TestCase("ServerDescription", EXPECTED_SERVER_DESCRIPTION)]
        [TestCase("ServerVersion", EXPECTED_SERVER_VERSION)]
        [TestCase("ServerVendorURL", EXPECTED_SERVER_VENDOR_URL)]
        public void Should_Create_And_Invoke_Grpc_Request_With_Dynamicly_Created_Types_At_Runtime(string property, string result)
        {
            // Arrange
            var propertyMap = new Dictionary<string, Type>() { { property, typeof(Sila2.Org.Silastandard.Protobuf.String) } };

            var grpcService = $"{_silaServiceFeature.Namespace}.{_silaServiceFeature.Identifier}";
            var featureProperties = _silaServiceFeature.GetDefinedProperties();
            var operationName = $"Get_{featureProperties.Single(x => x.Identifier.Equals(property)).Identifier}";

            var dynamicProtobufBuilder = new DynamicProtobufTypeBuilder();
            var dynProtoType = dynamicProtobufBuilder.Build("DynamicProtobufTestClass", propertyMap);

            var method = typeof(GrpcClient).GetMethods().Where(x => x.Name.StartsWith("BlockingUnary")).First();// ("BlockingUnary").MakeGenericMethod(typeof(EmptyMessage), typeof(Response));
            var genericMethod = method.MakeGenericMethod(typeof(EmptyMessage),dynProtoType);

            // System under Test
            var client = new GrpcClient(_testServerFixture.GrpcChannel.CreateCallInvoker(), grpcService);

            // Act
            dynamic response = genericMethod.Invoke(client, new object[] { new EmptyMessage(), operationName, null });

            // Assert
            switch(property)
            {
                case "ServerUUID":
                    Assert.That(response.ServerUUID.Value.ToUpper(), Is.EqualTo(result));
                    break;
                case "ServerName":
                    Assert.That(response.ServerName.Value, Is.EqualTo(result));
                    break;
                case "ServerType":
                    Assert.That(response.ServerType.Value, Is.EqualTo(result));
                    break;
                case "ServerVersion":
                    Assert.That(response.ServerVersion.Value, Is.EqualTo(result));
                    break;
                case "ServerVendorURL":
                    Assert.That(response.ServerVendorURL.Value, Is.EqualTo(result));
                    break;
                case "ServerDescription":
                    Assert.That(response.ServerDescription.Value, Is.EqualTo(result));
                    break;
                default: 
                    throw new NotImplementedException($"Unknown Property: {property}");
            }
        }

        [Test]
        public void Should_Create_And_Invoke_Grpc_Request_With_Dynamicly_Created_Types_With_Parameter_At_Runtime()
        {
            // Arrange
            const string EXPECTED_RESULT_SUBSTRING_PREFIX = "This Feature MUST be implemented by each SiLA Server";
            
            var operationName = $"GetFeatureDefinition";
            var parameter = "FeatureIdentifier";
            var responseProperty = "FeatureDefinition";
            var propertyMapRequest = new Dictionary<string, Type>() { { parameter, typeof(Sila2.Org.Silastandard.Protobuf.String) } };
            var propertyMapResponse = new Dictionary<string, Type>() { { responseProperty, typeof(Sila2.Org.Silastandard.Protobuf.String) } };

            var grpcService = $"{_silaServiceFeature.Namespace}.{_silaServiceFeature.Identifier}";
            var featureProperties = _silaServiceFeature.GetDefinedProperties();

            var dynamicProtobufBuilder = new DynamicProtobufTypeBuilder();
            var dynProtoTypeRequest = dynamicProtobufBuilder.Build("DynamicProtobufTestRequestClass", propertyMapRequest);
            var dynProtoTypeResponse = dynamicProtobufBuilder.Build("DynamicProtobufTestResponseClass", propertyMapResponse);

            var method = typeof(GrpcClient).GetMethods().Where(x => x.Name.StartsWith("BlockingUnary")).First();
            var genericMethod = method.MakeGenericMethod(dynProtoTypeRequest, dynProtoTypeResponse);

            // System under Test
            var client = new GrpcClient(_testServerFixture.GrpcChannel.CreateCallInvoker(), grpcService);

            // Act
            dynamic request = Activator.CreateInstance(dynProtoTypeRequest);
            request.FeatureIdentifier = new Sila2.Org.Silastandard.Protobuf.String { Value = "org.silastandard/core/SiLAService/v1" };
            dynamic response = genericMethod.Invoke(client, new object[] { request, operationName, null });

            // Assert
            Assert.That(response.FeatureDefinition.Value.IndexOf(EXPECTED_RESULT_SUBSTRING_PREFIX) > -1);
        }
    }
}
