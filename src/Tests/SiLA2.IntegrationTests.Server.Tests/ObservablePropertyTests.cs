﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Observablepropertytest.V1;
using GrpcCoreMetaData = Grpc.Core.Metadata;


namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    public class ObservablePropertyTests
    {
        private GrpcChannel _channel;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, new string[] { });

            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            _channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);
        }

        [Test]
        public void Subscribe_Editable_Works()
        {
            // Arrange
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            int[] testValues = { 0, 8, 15, 2345 };

            // System under Test
            var observablePropertyTestClient = new ObservablePropertyTest.ObservablePropertyTestClient(_channel);

            // Act & Assert

            // set initial value
            observablePropertyTestClient.SetValue(new SetValue_Parameters { Value = new Integer { Value = testValues[0] } });

            // set test values
            new Thread(() =>
            {
                for (int i = 1; i < testValues.Length; i++)
                {
                    autoResetEvent.WaitOne();
                    Console.WriteLine($"Value set: {testValues[i]}");
                    observablePropertyTestClient.SetValue(new SetValue_Parameters { Value = new Integer { Value = testValues[i] } });
                }
            }).Start();

            // subscribe to property Editable and read as many messages as defined in testValues
            var cts = new CancellationTokenSource();
            var responseStream = observablePropertyTestClient.Subscribe_Editable(new Subscribe_Editable_Parameters(), GrpcCoreMetaData.Empty, null, cts.Token).ResponseStream;

            var valuesReceived = 0;
            while (responseStream.MoveNext().Result)
            {
                Console.WriteLine($"Value: {responseStream.Current.Editable.Value}, Expected: {testValues[valuesReceived]}");
                Assert.That(responseStream.Current.Editable.Value, Is.EqualTo(testValues[valuesReceived]), "Received value does not match the set one");

                if (++valuesReceived == testValues.Length) { break; }
                autoResetEvent.Set();
            }

            cts.Cancel();
        }
    }
}
