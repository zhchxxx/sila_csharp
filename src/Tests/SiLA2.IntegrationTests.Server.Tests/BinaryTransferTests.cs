﻿using Google.Protobuf;
using Grpc.Core;
using Sila2.Org.Silastandard.Test.Binarytransfertest.V1;
using System.Text;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    public class BinaryTransferTests
    {
        private GrpcChannel _channel;

        private SiLAFramework.BinaryUpload.BinaryUploadClient _binaryUploadClient;
        private SiLAFramework.BinaryDownload.BinaryDownloadClient _binaryDownloadClient;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, new string[] { });

            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            _channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);

            _binaryUploadClient = new SiLAFramework.BinaryUpload.BinaryUploadClient(_channel);
            _binaryDownloadClient = new SiLAFramework.BinaryDownload.BinaryDownloadClient(_channel);
        }

        [Test]
        public void Deleted_BinaryUpload_Reference_Error()
        {
            // System under Test
            var binaryTransferTestClient = new BinaryTransferTest.BinaryTransferTestClient(_channel);

            // Act & Assert
            var binaryTransferUUID = DoBinaryUpload(Encoding.UTF8.GetBytes("A_somewhat_longer_test_string_to_demonstrate_the_binary_upload"), 5).Result;
            _binaryUploadClient.DeleteBinary(new SiLAFramework.DeleteBinaryRequest { BinaryTransferUUID = binaryTransferUUID });
            Assert.Catch(() => binaryTransferTestClient.EchoBinaryValue(new EchoBinaryValue_Parameters { BinaryValue = new SiLAFramework.Binary { BinaryTransferUUID = binaryTransferUUID } }));
        }
        
        private async Task<string> DoBinaryUpload(byte[] value, int chunkSize)
        {
            try
            {
                // create the binary and getting the binary transfer UUID
                var binaryTransferUUID = _binaryUploadClient.CreateBinary(new SiLAFramework.CreateBinaryRequest
                {
                    BinarySize = (ulong)value.Length,
                    ChunkCount = (uint)(value.Length / chunkSize + (value.Length % chunkSize != 0 ? 1 : 0)),
                    ParameterIdentifier = "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue"
                }).BinaryTransferUUID;

                // do the upload
                using (var call = _binaryUploadClient.UploadChunk())
                {
                    var chunkIndex = 0;
                    var offset = 0;
                    while (offset < value.Length)
                    {
                        var sentChunkSize = Math.Min(chunkSize, value.Length - offset);

                        await call.RequestStream.WriteAsync(new SiLAFramework.UploadChunkRequest
                        {
                            BinaryTransferUUID = binaryTransferUUID,
                            ChunkIndex = (uint)chunkIndex,
                            Payload = ByteString.CopyFrom(value, offset, sentChunkSize)
                        });

                        await call.ResponseStream.MoveNext();
                        if (call.ResponseStream.Current.BinaryTransferUUID != binaryTransferUUID)
                        {
                            throw new Exception($"Exception while uploading chunk: received binary transfer UUID '{call.ResponseStream.Current.BinaryTransferUUID}' differs from the sent one '{binaryTransferUUID}'");
                        }

                        if (call.ResponseStream.Current.ChunkIndex != chunkIndex)
                        {
                            throw new Exception($"Exception while uploading chunk: received chunk index {call.ResponseStream.Current.ChunkIndex} differs from the sent one ({chunkIndex})");
                        }

                        offset += sentChunkSize;
                        chunkIndex++;
                    }

                    await call.RequestStream.CompleteAsync();

                    //_logger.LogInformation($"{value.Length} bytes uploaded with {chunkIndex} chunks");

                    return binaryTransferUUID;
                }
            }
            catch (RpcException e)
            {
                //_logger.LogError(ErrorHandling.HandleException(e));
            }

            return null;
        }
    }
}
