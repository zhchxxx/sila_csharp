﻿using Google.Protobuf;
using Grpc.Core;
using Sila2.Org.Silastandard.Test.Metadataconsumertest.V1;
using Sila2.Org.Silastandard.Test.Metadataprovider.V1;
using SiLA2.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    public class ClientMetadataTests
    {
        private GrpcChannel _channel;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, new string[] { });

            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            _channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);
        }

        [Test]
        public void GetAffectedFCPTests()
        {
            // Arrange
            const int EXPECTED_NUMBER_OF_AFFECTED_FCP = 1;
            const string EXPECTED_AFFECTED_FCP_FOR_STRING_METADATA = "org.silastandard/test/MetadataConsumerTest/v1";
            const string EXPECTED_AFFECTED_FCP_FOR_INTEGERS_METADATA = EXPECTED_AFFECTED_FCP_FOR_STRING_METADATA + "/Command/UnpackMetadata";

            // System under Test
            var metadataProviderClient = new MetadataProvider.MetadataProviderClient(_channel);

            // Act
            var resultStringMetadata = metadataProviderClient.Get_FCPAffectedByMetadata_StringMetadata(new Get_FCPAffectedByMetadata_StringMetadata_Parameters());
            var resultIntegersMetadata = metadataProviderClient.Get_FCPAffectedByMetadata_TwoIntegersMetadata(new Get_FCPAffectedByMetadata_TwoIntegersMetadata_Parameters());

            // Assert
            Assert.That(resultStringMetadata.AffectedCalls, Has.Count.EqualTo(EXPECTED_NUMBER_OF_AFFECTED_FCP), $"[Get_FCPAffectedByMetadata_StringMetadata] wrong number of elements ({resultStringMetadata.AffectedCalls.Count}) in the list (expected: {EXPECTED_NUMBER_OF_AFFECTED_FCP})");
            Assert.That(resultStringMetadata.AffectedCalls[0].Value, Is.EqualTo(EXPECTED_AFFECTED_FCP_FOR_STRING_METADATA), $"[Get_FCPAffectedByMetadata_StringMetadata] wrong FQI ('{resultStringMetadata.AffectedCalls[0].Value}') received (expected: '{EXPECTED_AFFECTED_FCP_FOR_STRING_METADATA}')");
            Assert.That(resultIntegersMetadata.AffectedCalls, Has.Count.EqualTo(EXPECTED_NUMBER_OF_AFFECTED_FCP), $"[Get_FCPAffectedByMetadata_TwoIntegersMetadata] wrong number of elements ({resultIntegersMetadata.AffectedCalls.Count}) in the list (expected: {EXPECTED_NUMBER_OF_AFFECTED_FCP})");
            Assert.That(resultIntegersMetadata.AffectedCalls[0].Value, Is.EqualTo(EXPECTED_AFFECTED_FCP_FOR_INTEGERS_METADATA), $"[Get_FCPAffectedByMetadata_TwoIntegersMetadata] wrong FQI ('{resultIntegersMetadata.AffectedCalls[0].Value}') received (expected: '{EXPECTED_AFFECTED_FCP_FOR_INTEGERS_METADATA}')");
        }

        [Test]
        public void EchoMetadataTests()
        {
            // Arrange
            const string STRING_METADATA = "something_meaningful_to_be_sent_as_metadata";
            const int FIRST_INTEGER_METADATA = 5124;
            const int SECOND_INTEGER_METADATA = 4711;

            // Create metadata objects
            var stringMetadata = new Metadata
            {
                new Metadata.Entry(SilaClientMetadata.ConvertMetadataIdentifierToWireFormat("org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata"),
                    new Metadata_StringMetadata { StringMetadata = new SiLAFramework.String { Value = STRING_METADATA } }.ToByteArray())
            };
            var integersMetadata = new Metadata
            {
                new Metadata.Entry(SilaClientMetadata.ConvertMetadataIdentifierToWireFormat("org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata"),
                    new Metadata_StringMetadata { StringMetadata = new SiLAFramework.String { Value = STRING_METADATA } }.ToByteArray()),
                new Metadata.Entry(SilaClientMetadata.ConvertMetadataIdentifierToWireFormat("org.silastandard/test/MetadataProvider/v1/Metadata/TwoIntegersMetadata"),
                    new Metadata_TwoIntegersMetadata
                    {
                        TwoIntegersMetadata = new Metadata_TwoIntegersMetadata.Types.TwoIntegersMetadata_Struct
                        {
                            FirstInteger = new SiLAFramework.Integer { Value = FIRST_INTEGER_METADATA },
                            SecondInteger = new SiLAFramework.Integer { Value = SECOND_INTEGER_METADATA }
                        }
                    }.ToByteArray())
            };

            // System under Test
            var metadataConsumerTestClient = new MetadataConsumerTest.MetadataConsumerTestClient(_channel);

            // Act
            var resultStringMetadata = metadataConsumerTestClient.EchoStringMetadata(new EchoStringMetadata_Parameters(), stringMetadata);
            var resultIntegersMetadata = metadataConsumerTestClient.UnpackMetadata(new UnpackMetadata_Parameters(), integersMetadata);

            // Assert
            Assert.That(resultStringMetadata.ReceivedStringMetadata.Value, Is.EqualTo(STRING_METADATA), $"[EchoStringMetadata] wrong string metadata ('{resultStringMetadata.ReceivedStringMetadata.Value}') returned (expected: '{STRING_METADATA}')");
            Assert.That(resultIntegersMetadata.ReceivedString.Value, Is.EqualTo(STRING_METADATA), $"[UnpackMetadata] wrong string metadata ('{resultIntegersMetadata.ReceivedString.Value}') returned (expected: '{STRING_METADATA}')");
            Assert.That(resultIntegersMetadata.FirstReceivedInteger.Value, Is.EqualTo(FIRST_INTEGER_METADATA), $"[UnpackMetadata] wrong first integer metadata ('{resultIntegersMetadata.FirstReceivedInteger.Value}') returned (expected: '{FIRST_INTEGER_METADATA}')");
            Assert.That(resultIntegersMetadata.SecondReceivedInteger.Value, Is.EqualTo(SECOND_INTEGER_METADATA), $"[UnpackMetadata] wrong second integer metadata ('{resultIntegersMetadata.SecondReceivedInteger.Value}') returned (expected: '{SECOND_INTEGER_METADATA}')");
        }
    }
}
