﻿using Grpc.Core;
using Sila2.Org.Silastandard.Test.Authenticationtest.V1;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Features.Service.Implementations
{
    public class AuthenticationTestImpl : AuthenticationTest.AuthenticationTestBase
    {
        private readonly AuthorizationService _authorizationService;
        private readonly Feature _feature;

        public AuthenticationTestImpl(ISiLA2Server silaServer, AuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
            _feature = silaServer.ReadFeature(Path.Combine("Features", "AuthenticationTest-v1_0.sila.xml"));
            _authorizationService.AddAccessProtectedItems(new List<string>
            {
                _feature.GetFullyQualifiedCommandIdentifier("RequiresToken"),
                _feature.GetFullyQualifiedCommandIdentifier("RequiresTokenForBinaryUpload")
            });
        }

        public override Task<RequiresToken_Responses> RequiresToken(RequiresToken_Parameters request, ServerCallContext context)
        {
            _authorizationService.IsAuthorized(context.RequestHeaders);
            return Task.FromResult(new RequiresToken_Responses());
        }

        public override Task<RequiresTokenForBinaryUpload_Responses> RequiresTokenForBinaryUpload(RequiresTokenForBinaryUpload_Parameters request, ServerCallContext context)
        {
            if (_authorizationService.IsAuthorized(context.RequestHeaders))
            {
                if (request.BinaryToUpload == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_feature.GetFullyQualifiedCommandParameterIdentifier("RequiresTokenForBinaryUpload", "BinaryToUpload"), "BinaryToUpload not provided"));
                }
            }
            return Task.FromResult(new RequiresTokenForBinaryUpload_Responses());
        }
    }
}
