// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: UnobservablePropertyTest.proto
// </auto-generated>
#pragma warning disable 0414, 1591, 8981
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1 {
  public static partial class UnobservablePropertyTest
  {
    static readonly string __ServiceName = "sila2.org.silastandard.test.unobservablepropertytest.v1.UnobservablePropertyTest";

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static void __Helper_SerializeMessage(global::Google.Protobuf.IMessage message, grpc::SerializationContext context)
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (message is global::Google.Protobuf.IBufferMessage)
      {
        context.SetPayloadLength(message.CalculateSize());
        global::Google.Protobuf.MessageExtensions.WriteTo(message, context.GetBufferWriter());
        context.Complete();
        return;
      }
      #endif
      context.Complete(global::Google.Protobuf.MessageExtensions.ToByteArray(message));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static class __Helper_MessageCache<T>
    {
      public static readonly bool IsBufferMessage = global::System.Reflection.IntrospectionExtensions.GetTypeInfo(typeof(global::Google.Protobuf.IBufferMessage)).IsAssignableFrom(typeof(T));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static T __Helper_DeserializeMessage<T>(grpc::DeserializationContext context, global::Google.Protobuf.MessageParser<T> parser) where T : global::Google.Protobuf.IMessage<T>
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (__Helper_MessageCache<T>.IsBufferMessage)
      {
        return parser.ParseFrom(context.PayloadAsReadOnlySequence());
      }
      #endif
      return parser.ParseFrom(context.PayloadAsNewBuffer());
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters> __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_AnswerToEverything_Parameters = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses> __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_AnswerToEverything_Responses = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters> __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_SecondsSince1970_Parameters = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses> __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_SecondsSince1970_Responses = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses.Parser));

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses> __Method_Get_AnswerToEverything = new grpc::Method<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses>(
        grpc::MethodType.Unary,
        __ServiceName,
        "Get_AnswerToEverything",
        __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_AnswerToEverything_Parameters,
        __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_AnswerToEverything_Responses);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses> __Method_Get_SecondsSince1970 = new grpc::Method<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses>(
        grpc::MethodType.Unary,
        __ServiceName,
        "Get_SecondsSince1970",
        __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_SecondsSince1970_Parameters,
        __Marshaller_sila2_org_silastandard_test_unobservablepropertytest_v1_Get_SecondsSince1970_Responses);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.UnobservablePropertyTestReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of UnobservablePropertyTest</summary>
    [grpc::BindServiceMethod(typeof(UnobservablePropertyTest), "BindService")]
    public abstract partial class UnobservablePropertyTestBase
    {
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses> Get_AnswerToEverything(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses> Get_SecondsSince1970(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for UnobservablePropertyTest</summary>
    public partial class UnobservablePropertyTestClient : grpc::ClientBase<UnobservablePropertyTestClient>
    {
      /// <summary>Creates a new client for UnobservablePropertyTest</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public UnobservablePropertyTestClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for UnobservablePropertyTest that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public UnobservablePropertyTestClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected UnobservablePropertyTestClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected UnobservablePropertyTestClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses Get_AnswerToEverything(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return Get_AnswerToEverything(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses Get_AnswerToEverything(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_Get_AnswerToEverything, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses> Get_AnswerToEverythingAsync(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return Get_AnswerToEverythingAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses> Get_AnswerToEverythingAsync(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_Get_AnswerToEverything, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses Get_SecondsSince1970(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return Get_SecondsSince1970(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses Get_SecondsSince1970(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_Get_SecondsSince1970, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses> Get_SecondsSince1970Async(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return Get_SecondsSince1970Async(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses> Get_SecondsSince1970Async(global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_Get_SecondsSince1970, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected override UnobservablePropertyTestClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new UnobservablePropertyTestClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static grpc::ServerServiceDefinition BindService(UnobservablePropertyTestBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_Get_AnswerToEverything, serviceImpl.Get_AnswerToEverything)
          .AddMethod(__Method_Get_SecondsSince1970, serviceImpl.Get_SecondsSince1970).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static void BindService(grpc::ServiceBinderBase serviceBinder, UnobservablePropertyTestBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_Get_AnswerToEverything, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_AnswerToEverything_Responses>(serviceImpl.Get_AnswerToEverything));
      serviceBinder.AddMethod(__Method_Get_SecondsSince1970, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Parameters, global::Sila2.Org.Silastandard.Test.Unobservablepropertytest.V1.Get_SecondsSince1970_Responses>(serviceImpl.Get_SecondsSince1970));
    }

  }
}
#endregion
