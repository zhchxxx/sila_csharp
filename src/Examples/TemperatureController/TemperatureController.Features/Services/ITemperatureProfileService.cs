﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TemperatureController.Features.Database;

namespace TemperatureController.Features.Services
{
    public interface ITemperatureProfileService
    {
        IQueryable<TemperatureProfile> GetTemperatureProfiles();
        Task Execute(TemperatureProfile temperatureProfile, Action<double> changeTemperature);

        Task<TemperatureProfile> GetTemperatureProfile(int id);
        Task InsertTemperatureProfile(TemperatureProfile temperatureProfile);
        Task UpdateTemperatureProfile(TemperatureProfile temperatureProfile);
        Task DeleteTemperatureProfile(TemperatureProfile temperatureProfile);
    }
}
