using AnIMLCore;
using LiteDB;
using Microsoft.EntityFrameworkCore;
using SiLA2.AnIML.Services.Builder;
using SiLA2.AnIML.Services.Provider;
using SiLA2.AnIML.Services;
using SiLA2.Database.NoSQL;
using SiLA2.Database.SQL;
using SiLA2.Frontend.Razor.Services;
using SiLA2.Frontend.Razor.Services.UserManagement;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using SiLA2.Server;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Security;
using TemperatureController.Features.Database;
using TemperatureController.Features.Services;
using SiLA2.Utils.Config;
using SiLA2.Utils.Network;
using static Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController;
using SiLA2.Frontend.Razor;

var builder = WebApplication.CreateBuilder(args);
ConfigureServices(builder.Services, builder.Configuration);
var app = builder.Build();
ConfigureApplication(app);
app.Run();

static void ConfigureApplication(WebApplication app)
{
    if (!app.Environment.IsDevelopment())
    {
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.UseHttpsRedirection();

    app.UseStaticFiles();

    app.UseRouting();

    app.MapBlazorHub();
    app.MapFallbackToPage("/_Host");
}

static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
{
    services.AddRazorPages();
    services.AddServerSideBlazor();
    services.AddDbContext<IDbUserContext, UserDbContext>(x => x.UseSqlite(configuration.GetConnectionString("DefaultConnection")));
    services.AddDbContext<IDbTemperatureProfileContext, TemperatureDbContext>(x => x.UseSqlite(configuration.GetConnectionString("TemperatureServiceConnection")));
    services.AddScoped<IRepository<TemperatureProfile>, TemperatureProfileRepository>();
    services.AddScoped<ITemperatureProfileService, TemperatureProfileService>();
    services.AddScoped<IRepository<User>, UserRepository>();
    services.AddScoped<IUserService, UserService>();
    services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
    services.AddScoped<IGrpcClientProvider, GrpcClientProvider>();
    services.AddScoped<IFeatureMapper, FeatureMapper>();
    services.AddScoped<IServerDataProvider, ServerDataProvider>();
    services.AddSingleton<ICertificateProvider, CertificateProvider>();
    services.AddSingleton<ICertificateContext, CertificateContext>();
    services.AddSingleton<ICertificateRepository, CertificateRepository>();
    services.RegisterDocumentDatabase(configuration.GetConnectionString("AnIMLDocumentDatabase"), out ILiteDatabase docDb);
    services.RegisterDocumentDatabaseTypes<AnIMLType>(docDb);
    services.AddSingleton<IAnIMLRepository, AnIMLRepository>();
    services.AddSingleton<IAnIMLTechniqueRepository, AnIMLTechniqueRepository>();
    services.AddTransient<ISeriesTypeBuilder, SeriesTypeBuilder>();
    services.AddTransient<ISeriesTypeProvider, SeriesTypeProvider>();
    //TODO: Refactor IGrpcClientProvider
    ServerConfig serverConfig = new ServerConfig(configuration["ServerConfig:Name"],
                                                         Guid.Parse(configuration["ServerConfig:UUID"]),
                                                         configuration["ServerConfig:FQHN"],
                                                         int.Parse(configuration["ServerConfig:Port"]),
                                                         configuration["ServerConfig:NetworkInterface"],
                                                         configuration["ServerConfig:DiscoveryServiceName"]);
    services.AddSingleton<IServerConfig>(serverConfig);
    services.AddSingleton(x =>
    {
        var channel = x.GetRequiredService<IGrpcChannelProvider>().GetChannel(serverConfig.FQHN, serverConfig.Port, true);
        return new TemperatureControllerClient(channel.Result);
    });
    services.AddScoped<FileJsInterop>();
}