using Authentication.Server.App.Services;
using SiLA2.AspNetCore;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Network.Discovery;
using SiLA2.Server;
using SiLA2.Server.Interceptors;
using SiLA2.Server.Services;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.Reflection;
using SiLA2.Frontend.Razor.Services.UserManagement;
using Microsoft.EntityFrameworkCore;
using SiLA2.Database.SQL;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using Auth.Server.App.Services;
using Microsoft.AspNetCore.Server.Kestrel.Core;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddGrpc(options =>
{
    options.EnableDetailedErrors = true;
    options.Interceptors.Add<LoggingInterceptor>();
    options.Interceptors.Add<MetadataValidationInterceptor>();
    options.Interceptors.Add<ParameterValidationInterceptor>();
});
    
builder.Services.AddSingleton<MetadataManager>();

builder.Services.AddSingleton<ISiLA2Server, SiLA2Server>();
builder.Services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
builder.Services.AddTransient<INetworkService, NetworkService>();
builder.Services.AddSingleton<ServiceDiscoveryInfo>();
builder.Services.AddSingleton<ServerInformation>();
builder.Services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
builder.Services.AddScoped<IServerDataProvider, ServerDataProvider>();
builder.Services.AddSingleton<IServerConfig>(new ServerConfig(builder.Configuration["ServerConfig:Name"],
                                                              Guid.Parse(builder.Configuration["ServerConfig:UUID"]),
                                                              builder.Configuration["ServerConfig:FQHN"],
                                                              int.Parse(builder.Configuration["ServerConfig:Port"]),
                                                              builder.Configuration["ServerConfig:NetworkInterface"],
                                                              builder.Configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
    var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
    var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
builder.Services.ConfigureWritable<ServerConfig>(builder.Configuration.GetSection("ServerConfig"), configFile);

builder.Services.AddDbContext<IDbUserContext, UserDbContext>(x => x.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddScoped<IRepository<User>, UserRepository>();
builder.Services.AddScoped<IUserService, UserService>();

builder.Services.AddScoped<IAuthenticationInspector, AuthenticationInspector>();
builder.Services.AddSingleton<AuthorizationService>();

builder.Services.AddSingleton<ICertificateProvider, CertificateProvider>();
builder.Services.AddSingleton<ICertificateContext, CertificateContext>();
builder.Services.AddSingleton<ICertificateRepository, CertificateRepository>();

builder.WebHost.ConfigureKestrel(serverOptions =>
{
    var kestrelServerConfigData = args.GetKestrelConfigData(serverOptions.ApplicationServices);
    serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
    serverOptions.Listen(kestrelServerConfigData.Item1, kestrelServerConfigData.Item2, listenOptions => listenOptions.UseHttps(kestrelServerConfigData.Item3));
});

var app = builder.Build();

var env = app.Services.GetService<IWebHostEnvironment>();
var siLA2Server = app.Services.GetService<ISiLA2Server>();
var logger = app.Services.GetService<ILogger<Program>>();

app.InitializeSiLA2Features(siLA2Server);

app.MapGrpcService<SiLAService>();
app.MapGrpcService<AuthenticationService>();
app.MapGrpcService<AuthorizationService>();
app.MapGrpcService<GreeterService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

logger.LogInformation($"{siLA2Server.ServerInformation}");
logger.LogInformation("Starting Server Announcement...");
siLA2Server.Start();

app.Run();
