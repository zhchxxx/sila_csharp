﻿using Grpc.Core;
using SiLA2Framework = Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Examples.Greetingprovider.V1;
using SiLA2.Server;
using SiLA2;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;

namespace Authentication.Server.App.Services
{
    public class GreeterService : GreetingProvider.GreetingProviderBase
    {
        private readonly Feature _siLA2Feature;
        private readonly AuthorizationService _authorizationService;

        public GreeterService(ISiLA2Server siLA2Server, AuthorizationService authorizationService)
        {
            _siLA2Feature = siLA2Server.ReadFeature(Path.Combine("Features", "GreetingProvider-v1_0.sila.xml"));
            _authorizationService = authorizationService;
            _authorizationService.AddAccessProtectedItems(new List<string> { _siLA2Feature.GetFullyQualifiedCommandIdentifier("SayHello") });
        }

        public override Task<SayHello_Responses> SayHello(SayHello_Parameters request, ServerCallContext context)
        {
            if (_authorizationService.IsAuthorized(context.RequestHeaders))
            {
                return Task.FromResult(new SayHello_Responses { Greeting = new SiLA2Framework.String { Value = "Hello " + request.Name.Value } });
            }
            else
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(_siLA2Feature.GetFullyQualifiedDefinedExecutionErrorIdentifier("InvalidAccessToken"), "Access Token could not be validated !"));
                return Task.FromResult(new SayHello_Responses());
            }
        }

        public override Task<Get_StartYear_Responses> Get_StartYear(Get_StartYear_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StartYear_Responses { StartYear = new SiLA2Framework.Integer { Value = DateTime.Now.Year } });
        }
    }
}
