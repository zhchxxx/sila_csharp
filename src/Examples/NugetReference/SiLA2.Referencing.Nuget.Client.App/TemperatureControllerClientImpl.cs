﻿using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
using SiLA2.Server.Utils;
using SiLA2.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController;
using GrpcCoreMetaData = Grpc.Core.Metadata;

namespace SiLA2.ReferencingNuget.Client.App
{
    public class TemperatureControllerClientImpl
    {
        private readonly ILogger<TemperatureControllerClientImpl> _logger;
        private readonly TemperatureControllerClient _temperatureControllerClient;

        public IList<ClientBase> Clients { get; } = new List<ClientBase>();

        public TemperatureControllerClientImpl(GrpcChannel channel, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TemperatureControllerClientImpl>();
            _temperatureControllerClient = new TemperatureControllerClient(channel);
            Clients.Add(_temperatureControllerClient);
        }

        #region TemperatureController commands and properties

        public async Task ControlTemperature(double temperature, bool oneLineOutput = false)
        {
            try
            {
                // start command
                var cmdId = _temperatureControllerClient.ControlTemperature(new ControlTemperature_Parameters { TargetTemperature = new Real { Value = temperature } }).CommandExecutionUUID;
                Console.WriteLine("ControlTemperature command started...");

                // wait for command execution to finish
                using (var call = _temperatureControllerClient.ControlTemperature_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        var currentExecutionInfo = responseStream.Current;
                        var message = $"--> Command ControlTemperature    -status: {currentExecutionInfo.CommandStatus}   -remaining time: {currentExecutionInfo.EstimatedRemainingTime?.Seconds,3:###}s    -progress: {FormatProgress(currentExecutionInfo.ProgressInfo.Value)}";
                        Console.ForegroundColor = ConsoleColor.DarkMagenta;
                        if (oneLineOutput)
                        {
                            Console.Write("\r" + message);
                        }
                        else
                        {
                            Console.WriteLine(message);
                        }
                        if (currentExecutionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || currentExecutionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write("\n");
                }

                // get result (empty response here, so this is not necessary - just for demonstration)
                ControlTemperature_Responses response = _temperatureControllerClient.ControlTemperature_Result(cmdId);
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Calls the ControlTemperature command but does not use ControlTemperature_Info to wait for the execution to be finished (calls ControlTemperature_Result immediately)
        /// </summary>
        /// <param name="temperature">The target temperature</param>
        public void ControlTemperatureNoWait(double temperature)
        {
            Console.WriteLine("Trying to get the result right after the ControlTemperature command has been started...");

            try
            {
                var cmdId = _temperatureControllerClient.ControlTemperature(new ControlTemperature_Parameters { TargetTemperature = new Real { Value = temperature } }).CommandExecutionUUID;
                ControlTemperature_Responses response = _temperatureControllerClient.ControlTemperature_Result(cmdId);
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subscribes to the CurrentTemperature property and listens endless.
        /// </summary>
        public async Task GetCurrentTemperature()
        {
            try
            {
                var responseStream = _temperatureControllerClient.Subscribe_CurrentTemperature(new Subscribe_CurrentTemperature_Parameters()).ResponseStream;

                while (await responseStream.MoveNext())
                {
                    WriteTemperatureString(responseStream.Current.CurrentTemperature.Value);
                }
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subscribes to the CurrentTemperature property for the given duration. The call is cancelled afterwards.
        /// </summary>
        public async Task GetCurrentTemperature(TimeSpan duration)
        {
            try
            {
                var cts = new CancellationTokenSource();
                var responseStream = _temperatureControllerClient.Subscribe_CurrentTemperature(new Subscribe_CurrentTemperature_Parameters(), GrpcCoreMetaData.Empty, null, cts.Token).ResponseStream;

                var startTime = DateTime.Now;
                while (await responseStream.MoveNext() && DateTime.Now - startTime < duration)
                {
                    WriteTemperatureString(responseStream.Current.CurrentTemperature.Value);
                }

                cts.Cancel();
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subscribes to the CurrentTemperature property and receives the given number of values before the stream will be cancelled.
        /// </summary>
        public async Task<double> GetCurrentTemperature(int numberOfValues)
        {
            double temperature = -1;
            try
            {
                var cts = new CancellationTokenSource();
                var responseStream = _temperatureControllerClient.Subscribe_CurrentTemperature(new Subscribe_CurrentTemperature_Parameters(), GrpcCoreMetaData.Empty, null, cts.Token).ResponseStream;

                var valueReceived = 0;
                while (await responseStream.MoveNext() && valueReceived++ < numberOfValues)
                {
                    temperature = responseStream.Current.CurrentTemperature.Value;
                    WriteTemperatureString(temperature);
                }

                cts.Cancel();
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
            return temperature;
        }

        #endregion

        #region Helper methods

        private static void WriteTemperatureString(double temperature)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("--> Property CurrentTemperature: {0:0.0} °C   ({1:0.0} °F)", temperature.Kelvin2DegreeCelsius(), temperature.Kelvin2DegreeFahrenheit());
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static string FormatProgress(double progress)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0,3:###}%  [", Math.Round(progress * 100.0));
            for (int i = 0; i < 10; i++)
            {
                sb.Append((int)Math.Round(progress * 10.0) > i ? "#" : " ");
            }
            sb.Append("]");

            return sb.ToString();
        }

        #endregion
    }
}
