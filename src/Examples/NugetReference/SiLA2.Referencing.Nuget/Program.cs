using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.AspNetCore;
using System.Reflection;
using System;
using SiLA2.Server;
using SiLA2.ReferencingNuget.Features.Services;
using Microsoft.Extensions.Configuration;
using SiLA2.Simulation;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.IO;
using SiLA2.Server.Interceptors;
using SiLA2.Server.Services;
using SiLA2.Commands;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.Config;

namespace SiLA2.ReferencingNuget.Server.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder();
            ConfigureServices(builder.Services, builder.Configuration);
            builder.WebHost.ConfigureKestrel(serverOptions =>
            {
                var kestrelServerConfigData = args.GetKestrelConfigData(serverOptions.ApplicationServices);
                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                serverOptions.Listen(kestrelServerConfigData.Item1, kestrelServerConfigData.Item2, listenOptions => listenOptions.UseHttps(kestrelServerConfigData.Item3));
            });
            var app = builder.Build();
            ConfigureApplication(app);

            app.Run();
        }

        private static void ConfigureApplication(WebApplication app)
        {
            var env = app.Services.GetService<IWebHostEnvironment>();
            var siLA2Server = app.Services.GetService<ISiLA2Server>();
            var logger = app.Services.GetService<ILogger<Program>>();

            app.InitializeSiLA2Features(siLA2Server);

            app.MapGrpcService<SiLAService>();
            app.MapGrpcService<TemperatureControllerService>();

            logger.LogInformation($"{siLA2Server.ServerInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<LoggingInterceptor>();
                options.Interceptors.Add<MetadataValidationInterceptor>();
                options.Interceptors.Add<ParameterValidationInterceptor>();
            });
            services.AddSingleton<MetadataManager>();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<IThermostatSimulator, ThermostatSimulator>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<TemperatureControllerService>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<ICertificateContext, CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<IServerConfig>(new ServerConfig(configuration["ServerConfig:Name"],
                                                                Guid.Parse(configuration["ServerConfig:UUID"]),
                                                                configuration["ServerConfig:FQHN"],
                                                                int.Parse(configuration["ServerConfig:Port"]),
                                                                configuration["ServerConfig:NetworkInterface"],
                                                                configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(configuration.GetSection("ServerConfig"), configFile);
        }
    }
}
