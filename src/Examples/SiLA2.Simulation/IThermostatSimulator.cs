﻿using System.Threading;

namespace SiLA2.Simulation
{
    public interface IThermostatSimulator
    {
        double CurrentTemperature { get; }
        double TargetTemperature { get; }

        void SetTargetTemperature(double temperature, CancellationToken cancellationToken);
    }
}